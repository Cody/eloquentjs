# Exploring Google Trends
---

# [Chocolate Trends](https://trends.google.com/trends/explore?date=today%205-y&geo=US&q=%2Fm%2F020vl)
## First Observations:

There is a steady amount of searches for chocolate except for three to four times a year, when searches increase significantly.

### These dates correspond to:

- Thanksgiving
- Christmas
- Valentine's Day
- Easter

Christmas is by far the largest spike, and Easter is the smallest.
---

### The first four states with the most searches for chocolate are:

1. Vermont
2. New Hampshire
3. Maine
4. New York

This seems to suggest that chocolate is more popular in New England than the rest of the country.
---

## Google Trends allows us to see related search terms that are rising in popularity.

1. hot chocolate bombs (*Breakout*)
2. how to make hot chocolate bombs (*Breakout*)
3. keto chocolate (*+2350%*)
4. chocolate bombs (*+1850%*)
5. hot chocolate bombs recipe (*+1500%*)

### This data shows us that hot chocolate bath bombs are a major trend.
---

# [World Cup Trends](https://trends.google.com/trends/explore?date=all&q=World%20Cup)

### The World Cup is most searched for in Brazil and surrounding countries like Argentina, because they are the biggest soccer fans and the most successful countries in the World Cup. Interestingly, though, there are some other countries that search for the World Cup more:

1. Nepal
2. Argentina
3. Lebanon
4. Panama
5. Bangladesh

---

### There are also some smaller peaks after each Soccer World Cup, and these correspond to the Cricket World Cup.

### The 2019 Cricket World Cup was particularly searched-for. We tried to figure out why, and noticed that this year's Cricket World Cup was held in the UK, which might have attracted more attention.

### There is also an even smaller peak slightly after for the Rugby World Cup.
