# Metadata in Google Trends

## Our topic: calculator

---

## Trends:

Every year, there is a quick, sharp dip in searches for calculators. This dip is always the lowest in the time period around December 25 (Christmas). There is also a more gradual, less dramatic dip every year roughly between early/mid-May to late-August/early-September.

### Is this causation?

We know that these dips correspond to the times when kids are on break. It is possible that they are not searching for calculators as much because they aren't in school, but we need proof. This data comes from Google, based on the volume of searches during different time periods, but beyond that, Google does not provide extra metadata. Without knowing how the data is collected, we can't conclude that this is causation.
---

# Code.org

## Topic: AQI Index by US County

### This data came from the EPA (United States Environmental Protection Agency) but they got *their* data from AirNow.gov

![Data Graph](/Users/1008972/Desktop/Screen\ Shot\ 2022-01-24\ at\ 9.05.16\ AM.png) 

There is a visible line of data points stretching from the origin out diagonally towards the top-left. (Positive correlation) The equation for this line is y = x. (In other words, for the data points on this line, the total number of recorded days is the same as the number of *good* days. This means that all of the recorded days in these states were *good* days.) Anything above the line means that those states had fewer *good* days than total recorded days. (Not all of their days were *good*.)

## About the data:

The AirNow website says:
"Measurements are collected by state, local or tribal monitoring agencies using federal reference or equivalent monitoring methods approved by EPA. Although preliminary data quality assessments are performed, the data in AirNow are not subjected to the full validation used to officially submit and certify data in EPA’s regulatory database - the Air Quality System (AQS). AQS data are used for regulatory purposes, such as determining attainment of the National Ambient Air Quality Standards (NAAQS), while AirNow data are used only to report the AQI to the public.

AirNow maps and AQI readings use only ozone, PM10, and PM2.5 at this time. However, once real-time calculations for other pollutants are developed, AirNow may offer real-time data for more pollutants. Some locations do provide air quality forecasts for NO2 and CO in addition to PM and ozone.

Most data arrive by half-past the previous hour and are quality assured and released by the end of the hour. For instance, an AirNow value shown as “1 pm EST” would have been measured in a one-hour sample from noon to 1 pm EST and would show up on AirNow around 2 pm EST."

### Based on this, it is safe to assume that the data is accurate and it is causation.
