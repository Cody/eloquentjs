### Names: Cody, Anar, Henry, Anthony

### As our example, we are going to say that you are trying to Google something and you just pressed enter on the search bar.

---

When you hit enter, you send a request to Google for the information that you searched for, and Google uses an algorithm to search through their database and look for the most relevant information based on factors like your location, previous search history/interests, and of course the search terms themselves.

---

## Routing

1. This request is sent from your device to your Wi-Fi router or a cell tower, which connects your request to the wider internet.
2. Your request is sent with the IP address of where it needs to go, so that DNSs (domain name servers) can route it to Google.
3. This information is mostly sent through fiber-optic cables from DNS to DNS.
4. Google then runs its algorithm and decides what information to send back.
5. Google breaks this information into packets and sends them individually back to you.
6. Once you get them all, you can put them together and the information will finally display on the webpage.

---

## Encryption

### This information is protected by encryption.

- Every device has two keys: a private key and a public key, which are used to encrypt and decrypt.
- The public key is available to anyone on the internet, while the private key is kept a secret.
- When you send a message to someone else, you use THEIR public key to encrypt the data, and they use their secret, private key to decrypt it again when they get it.
- This keeps anyone who is not supposed to read the data from being able to do so.
- This works because it is much easier to create a public key from a private key than it is to derive a private key from a public key.
