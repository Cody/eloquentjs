onEvent("DNAinput", "input", function( ) {
  setProperty("DNAinput", "text-color", "black");
  var input = getText("DNAinput").toUpperCase();
  var mRNAoutput = transcription(input);
  translation(mRNAoutput);
});

function transcription(DNA){
  var mRNAlist = [];
  for (var i = 0; i < DNA.length; i++){
    switch (DNA[i]){
      case "A":
        appendItem(mRNAlist, "U");
        break;
      case "T":
        appendItem(mRNAlist, "A");
        break;
      case "G":
        appendItem(mRNAlist, "C");
        break;
      case "C":
        appendItem(mRNAlist, "G");
        break;
      case " ":
        continue;
      default:
      setProperty("DNAinput","text-color","red");
    }
  }
  for (var y = 3; y < mRNAlist.length; y += 4){
      insertItem(mRNAlist, y, " ");
  }
  var mRNA = mRNAlist.join("");
  setText("mRNAoutput", mRNA);
  console.log(mRNA);
  return mRNA;
}

function translation(mRNA){
  var AminoAcids = "";
  for (var i = 0; i < mRNA.length; i += 4){
    switch (mRNA.substring(i, i + 3)){
      case "AUG":
        AminoAcids += "Met-";
        break;
      case "UCU": case "UCC": case "UCA": case "UCG": case "AGU": case "AGC":
        AminoAcids += "Ser-";
        break;
      case "UUU": case "UUC":
       AminoAcids += "Phe-";
       break;
      case "UUA": case "UUG": case "CUU": case "CUC": case "CUA": case "CUG":
      	AminoAcids += "Leu-";
      	break;
      case "AUU": case "AUC": case "AUA":
       AminoAcids += "Ile-";
       break;
      case "GUU": case "GUC": case "GUA": case "GUG":
	      AminoAcids += "Val-";
	      break;
	    case "CCU": case "CCC": case "CCA": case "CCG":
	      AminoAcids += "Pro-";
	      break;
      case "ACU": case "ACC": case "ACA": case "ACG":
	      AminoAcids += "Thr-";
      	break;
      case "GCU": case "GCC": case "GCA": case "GCG":
	      AminoAcids += "Ala-";
	      break;
      case "UAU": case "UAC":
	      AminoAcids += "Tyr-";
	      break;
      case "CAU": case "CAC":
	      AminoAcids += "His-";
	      break;
      case "AAA": case "AAG":
	      AminoAcids += "Lys-";
	      break;
      case "GAU": case "GAC":
      	AminoAcids += "Asp-";
      	break;
      case "GAA": case "GAG":
      	AminoAcids += "Glu-";
      	break;
      case "UGU": case "UGC":
       	AminoAcids += "Cys-";
      	break;
      case "UGG":
	      AminoAcids += "Trp-";
      	break;
      case "CGU": case "CGC": case "CGA": case "CGG": case "AGA": case "AGG":
	      AminoAcids += "Arg-";
      	break;
      case "AAU": case "AAC":
      	AminoAcids += "Asn-";
      	break;
      case "CAA": case "CAG":
	      AminoAcids += "Gln-";
      	break;
      case "GGU": case "GGC": case "GGA": case "GGG":
      	AminoAcids += "Gly-";
      	break;
      case "UAA": case "UAG": case "UGA":
        AminoAcids += "Stop-";
        break;
      default:
	      setText("AAoutput", "Cannot recognize \"" + AminoAcids.substring(i, i + 3) + "\"");
    }
  }
  AminoAcids = AminoAcids.slice(0, AminoAcids.length-1);
  setText("AAoutput", AminoAcids);
  console.log(AminoAcids);
}
