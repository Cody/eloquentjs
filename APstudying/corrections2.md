# Test Corrections 2

## Question 1: Rogue Access Point Definition (Moderate)

Which of the following is **NOT** completely true about a rogue access point?

**Category: 5F (Safe Computing)**

### My answer:

Company employees may install a rogue access point to give other employees easy access to the company’s network.

This is wrong because it *is* likely that an employee would set up a rogue access point for other employees.

### Correct answer:

The intent of a rogue access point is for either an attacker or hacker to gain access to the network.

This is right because it is **not** *just* hackers can install a rogue access point. It can also be done by a well-meaning employee, like in the answer I selected.

---

## Question 2: Citizen Science Examples (Difficult)

Of the following, which is the best example of citizen science?

**Category: 1A (Collaboration)**

### My answer:

A soft drink company conducts a poll asking their social media followers, who have tried their latest soda, to give a thumbs up or thumbs down on its taste. The company posts the cumulative results on its website.

This is wrong because this is not scientific research, so it can't be considered citizen science. Instead, this shoudl be considered crowdsourcing.

### Correct answer:

A state agency conducts a study to examine the population of birds across the state. On New Year’s Day, volunteers are asked to count and classify the different birds on their property and transmit this information to a central database.

This *is* citizen science. It is using ordinary people to conduct scientific research more easily through numbers.

---

## Question 3: Number of Iterations For Binary Search

Suppose a list of numbers called dataset contains 2000 values. If the binary search algorithm was used to search the list for a value, what would be the maximum number of iterations necessary to successfully find this known value?

**Category: 3I (Binary Search)**

## My answer:

2000

I remember what I was thinking while i was taking the test: I was thinking that by "binary search" they meant as opposed to quantum search (quantum computers can do menial searches like that *far* quicker than binary computers). What I didn't know was the difference between a sequential search and binary search.

[This website](https://www.geeksforgeeks.org/binary-search/) helped me understand binary searches.

## Correct answer:

11

The best way to get to this answer is to use a binary to decimal table to visualise doubling the number each time. This is because the algorithm will divide the remaining part of an ordered list into two and pick the value in the middle, then repeat until it finds the right value. With large, ordered datasets, this is more efficient than sequential searching. When the value of the spot is more than the length of the array, the position of that spot is the maximum number of iterations to find the value. 
