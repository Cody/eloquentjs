# My Study Journal

## Test Corrections

### Question 8:

My answer: **B**

Correct answer: **D**

Explanation: It is all three of the answers.

### Question 14:

My answer: **B**

Correct answer: **D**

Explanation: It is more efficient to filter out employees with the job title "Manager" (not manually though) than to sort. And it more efficient to sort the column and delete entries <0 than to do it manually.

### Question 29:

My answer: **D**

Correct answer: **A**

Explanation: I thought it was the *most* likely advantage of a driving simulator, but it was actually the *least* likely advantage. Which explains why it was how it physically feels to get into a crash.

### Question 46:

My answer: **C**

Correct answer: **D**

Explanation: I was stupid and I thought that the print statements at the end were printing a and b for some reason, not a and c.

### Question 49:

My answer: **B**

Correct answer: **A**

Explanation: I went through the binary number by number. I did the last number first to try to narrow it down, but I must have done that part wrong.

### Question 51:

My answer: **C**

Correct answer: **D**

Explanation: I didn't know that you needed an open source license to use it, or that work on the Internet is automatically public domain.

### Question 54:

My answer: **A**

Correct answer: **C**

Explanation: It was the part about the position that was confusing me. I didn't understand that incrementing the position also meant repeating step 1.

### Question 55:

My answer: **A**

Correct answer: **D**

Explanation: I actually did so bad on this question that I got the opposite of the right answer. I said a good way to keep the cost down would be to store the images without compression, because they said one of the criteria was not losing quality. But the right answers were the other two - to make all the images lossy so it is quicker to download them, and make them lossy but without losing quality that would be visible to the human eye.

### Question 57:

My answer: **A**

Correct answer: **B**

Explanation: It allows ordinary people to contribute scientific research without needing expertise while being organized by scientists. I think the key is that the data is *analyzed* by scientists.

### Question 68:

My answer: **A, C**

Correct answer: **A, D**

Explanation: Answer C multiplied the result by 10 once when it the variable spin2 was assigned, and again when it was printed, so it was actually multiplied by 100.

---

## Terms Research
While I was researching these terms, I also completed practice problems associated with them.

### Big Idea 1:

**Citizen science:**
Scientific research conducted largely by amateur scientists. Public participation in scientific research.

### Big Idea 2:

**Lossy and Lossless Compression:**
Lossy compression is compression that makes the file lose information or quality. Lossless compression reduces the file size without losing any information.

**Consolidation:**
When data storage or server resources are shared among multiple users and accessed by multiple applications.

### Big Idea 3:

**Data Abstraction:**
Reduction of a particular body of data to a simplified representation of the whole.

**Procedural Abstraction:**
Idea that each method should have a coherent conceptual description that separates its implementation from its users.

**Heuristic:**
Enabling someone to learn or discover something for themselves. A technique designed for solving a problem more quickly when classic methods are too slow, or for finding an approximate solution when classic methods fail to find any exact solution.

**Decidable:**
Problem can be solved every time using an algorithm.

**Undecidable:**
Problem can be solved some of the time:

**Unsolvable:**
Problem can never be solved with an algorithm.

### Big Idea 4:

**Scalability:**
The ability of a system to adjust in scale to meet new demands.

**TCP:**
A data transport protocol that includes mechanisms for reliably transmitting packets to a destination.

**Parallel Computing:**
A computational model which splits a program into multiple tasks, some of which can be executed simultaneously.

**Distributed Computing:**
A computational model which uses multiple devices to run different parts of a program.

### Big Idea 5:

**Creative Commons:**
An alternative to copyright that allows people to declare how they want their artistic creations to be shared, remixed, used in noncommercial contexts, and how the policy should propagate with remixed versions.

**Open Access:**
A policy that allows people to have access to documents (like research papers) for reading or data (like government datasets) for analysis.
