# Data That Customer Provides in Original But Not New System

## Category: 1B (Program Function and Purpose)

### How to tell:

The first thing to do is *narrow down the options*, starting with the larger categories.

It does not have anything to do with **data**, or **computing systems and networks**, so sections **2** and **4** can immediately be eliminated. Looking through sections **3** and **5**, the subsections appear not to be related either, so we can also eliminate those sections, too. That leaves just **section 1**, **Creative Development**. *Collaboration* and *Identify and Correcting Errors* don't sound quite right to me. Between *Program Function and Purpose* and *Program Design and Devlopment*, I decided *Program Function and Purpose* sounded more fitting, since the question was about how the two systems worked and not the visual interface or how the new system was developed.

---

## Question:

**These are the exact words of the question on Albert.io:**

A toy company has recently upgraded its system for taking customer orders electronically. The new system allows the customer to select the toys that they would like to order from available inventory via a web page on the company website. The original system only allowed the customer to search for the toys they wanted to purchase using a paper catalog book and then email the company’s customer service department to let them know which toys they wanted to purchase.

The upgraded system stores the company’s product information in a database and each toy is identified by a unique product id. The database includes data such as toy description, unit price, available inventory levels, as well as other important attributes of the toy. This information can be accessed by customers via specialized software customized for the company. The customer can view the toy inventory, choose the toys they want to purchase, and place them into their personal electronic cart.


Picture of the flowchart provided:

I couldn't figure out how to put a picture of the flowchart in this document, but here is a version of it in exclusively written form:

### Original System:

1. Cutomer finds desired toys in paper catalog
2. Customer sends an email to Customer Service Dept listing the product IDs and quantities of the toys they wish to purchase
3. Test: Are all requested quantities of toys available?

**If yes**:
1. Customer is sent a bill via email for the entire order
2. Customer sends payment via US postage

**If no**:
1. Customer is sent an email notifying them that the order cannot be filled.

### New System:

1. If first-time user, customer creates new login ID/password for toy website. Customer logs onto website and browses pictures of toys.
2. Customer clicks on image of the toy they wish to purchase.
3. Test: Is the toy available?

**If yes**:
1. Total quantity available and current unit price displayed.
2. Customer enters the quantity they want to order.
3. Electronic cart is updated with order information

**If no**:
1. Message stating that "**Toy is out of stock**" is displayed on the screen.

4. Test: Does the cutomer want to order ***other*** toys?

**If yes**:
1. Go to **step 2**

---

## Corrections & Solution:

### Possible Answers:

Which of the following data must be provided by the customer in the original system, but is **NOT**  directly provided by the customer in the upgraded new system?

**A:** Order quantity of the toy being ordered

**B:** Current inventory level of the toy being ordered

**C:** Description of the toy being being ordered
(my answer)

**D:** Product ID of the toy being ordered
(correct answer)

### Why I Got the Question Wrong:

The main reason I got the question wrong is just that I was rushing and being sloppy I didn't actually read the flowchart carefully. If I had, I would have seen that in the original system, in the second block it says: "*Customer sends an email to Customer Service Dept listing the ****product IDs and quantities**** of the toys they wish to purchase.*"

Instead, I was just guessing what information would be needed in the previous system based on prior knowledge. My thought process went somewhat as such: *The website would need to submit the product ID to the database for processing, right? So it can't be that.* This also relates to reading the *question* carefully because there is one key word that I missed: **directly**. The *website* does have to know the product ID, but the *customer* does not, at least not in the new system. That's why it is the right answer, because the customer *does* have to know the product ID in the first system, but *not* in the new one.

### How to Avoid Getting Similar Questions Wrong in the Future:

For *me*, the best way to avoid having a similar problem in the future would just be to read both the *question* and the *material* more carefully to avoid silly answers. I didn't even realise that I was supposed to look for the answer in the flowchart as opposed to just making an educated guess about the answer.

However, that may not be the case for others who got the same question wrong, as their mistake may not be so trivial. Some people who had to do this question may not have even had to use flowcharts before. So, [here](https://www.gliffy.com/blog/guide-to-flowchart-symbols) is a website that explains how to use flowcharts. I will go through this website with the class to explain it all.

### Understanding Other Questions in This Category

We can use [these](https://www.albert.io/learn/ap-computer-science-principles/12-program-function-and-purpose--1/program-functionality) Albert.io questions to practice more of this type of question.

**Key things to know for the future:**

- Purpose refers to the reason software exists
- Functionality refers to the way the user interacts with software, *not* how the software works

Other than that, you kind of just have to know the random bits of information you might need, and there is no way to anticipate what those will be.
