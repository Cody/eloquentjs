# Ten New Vocabulary Terms

**My list:**
1. Citizen science
2. Crowdsourcing
3. Difference reduction
4. Rogue access point
5. Binary search
6. Distributed computing

(Some of the questions I got wrong tended not to be vocabulary-centric but rather were about algorithms or datasets, so I didn't have 10 words that were technical and that people wouldn't already know.)

---

## Citizen science
### Question: Ice Cream Company Wants a New Flavor

**Wikipedia Defintion:**
Citizen science (CS; also known as community science, crowd science, crowd-sourced science, civic science, or volunteer monitoring) is scientific research conducted, in whole or in part, by amateur (or nonprofessional) scientists.

**Question:**
A major ice cream asked which new flavor people would create via a social media post. Which of the following best describes the problem-solving process that the ice cream company is using?

**My answer:**
Citizen science

The reason I got this question wrong is that a poll for a new ice cream flavor cannot be considered *scientific research*.

## Crowdsourcing
### Question: Ice Cream Company Wants a New Flavor

**Wikipedia Definition:**
Crowdsourcing is a sourcing model in which individuals or organizations obtain goods or services - including ideas, voting, micro-tasks, and finances - from a large, relatively open, and often rapidly evolving group of participants.

**Question:** *same as above*

**Correct answer:** Crowdsourcing
**This was the correct answer to the question.**

The reason this answer is right is that the ice cream company wants to *obtain ideas* from their *group of particpants* (their fans who responded to the social media survey).

## Difference reduction
### Question: Ice Cream Company Wants a New Flavor

**Study.com Defintion:** (*Wikipedia didn't have a definition*)
Difference reduction requires you to break down a large task into smaller steps. The first thing you do is ask yourself what step will take you from where you are to as close as possible to the final goal. You take that step and repeat the process until you finally reach the goal.

**Question:** *same as two above*

Differnce Reduction
**This was an incorrect answer to the question.**

The reason this answer is wrong is that difference reduction is a way to approach a formidable task by breaking it into smaller increments, but the ice cream company was collecting data from users. Although they did plan to have a vote to narrow down the results, that is not the same thing as repeating a smaller step multiple times until you reach your goal.

## Rogue access point
### Question: Rogue Access Point Definition

**Wikipedia Definition:**
A rogue access point is a wireless access point that has been installed on a secure network without explicit authorization from a local network administrator, whether added by a well-meaning employee or by a malicious attacker.

**Question:**
Which of the following is **NOT** completely true about rogue access points?

**My answer:** Company employees may install a rogue access point to give other employees easy access to the company's network.

**Why I got it wrong:**
This *is* completely true, well-meaning employees often do this to try to make accessing a network easier for others, but it can also make a network less secure.

**Correct answer:**
The intent of a rogue access point is for either an attacker or hacker to gain access to a company's network.

**Why that answer is correct:**
That is *NOT* always the reason why a rogue access point is intalled. Employees may *ALSO* install one out of purely good intentions, making this answer correct.

## Binary search
### Question: Number of Iterations for binary Search

**Wikipedia Definition:**
In computer science, binary search, also known as half-interval search, logarithmic search, or binary chop, is a search algorithm that finds the position of a target value within a sorted array. Binary search compares the target value to the middle element of the array. If they are not equal, the half in which the target cannot lie is eliminated and the search continues on the remaining half, again taking the middle element to compare to the target value, and repeating this until the target value is found. If the search ends with the remaining half being empty, the target is not in the array.

The most important part of this definition is just the first sentence. It is a very efficient way of finding a value in a list, but it only works if the list is ordered.

**Question:**
What would be the maximum number of iterations necessary to find a value in an ordered list of 2000 items?

**My answer:** 2000

**Why I chose this answer:**
I didn't understand what a binary search was. I hadn't heard the term before, so I assumed it was an iterative search (*I also didn't know that term at the time*).

**Why I got the question wrong:**
Binary search divides a list into two parts and picks the index in the very middle of that set. It determines whether the value it is looking for is less or greater than the value at its current point, readjusts so it is only looking within the remaining half of the list, and repeats until it finds the value.

**Correct answer:** 11

**Why this is the correct answer:**
You can tell the maximum number of iterations by using a binary to decimal chart:

Binary		2^12	2^11	2^10	2^9		2^8		2^7		2^6		2^5		2^4		2^3		2^2		2^1
Decimal		2048	1024	512		256		128		64		32		16		8		4		2		1

You can represent the number 2000 with 11 bits, so that's also the maximum number of iterations.

## Distributed computing
### Question: Most Benefitted From Using Distributed Computing

**Wikipedia Definition:**
A distributed system is a system whose components are located on different networked computers, which communicate and coordinate their actions by passing messages to one another from any system. The components interact with one another in order to achieve a common goal.

**Question:**
Which of the following would be most benefitted by using distributed computing?

I did get one of the correct answers, but I missed that the question asked for two. I will explain both of the answers.

**The one I got:**
Operating an online game with hundreds of virtual players participating.

**Why it's right:**
It makes more sense to have players doing processing on their own computers than to have it all on one central server and then relay the processed information feeds to all the players. It *needs* to rely on distributed computing because the output is on many different computers all at once, and being presented in different ways.

**The one I missed:**
Controling the aircraft that depart and arrive at an international airport.

**Why it's right:**
An aircraft control system relies an data from many places to track and manage incoming planes. It also needs to store this enormous amount of data in several different places in order for it to work properly.

**Process of elimination:**
It would not be computing the average of a list of 20 million numbers because there is no benefit to doing this processing in many different places.

It is not determining how many letter "a"-s are in a thick book because although that may take a while, it is not necessary to use distributed computing.
