# Correcting the questions I got wrong on the AP practice test

## Question 1: Is a Teenager (Moderate)

A teenager is a person who is between 13 and 19 years old (including 13 and 19).

Which of the following expressions does NOT correctly display whether a person is a teenager or not?

### My answer:

IF ((NOT age < 13) AND (NOT age > 19))
{
	DISPLAY("Teenager")
}
ELSE
{
	DISPLAY("Not a Teenager")
}

This answer *does* work (meaning it is wrong) because NOT age < 13 is like age ≥ 13, which is what it is supposed to be. And the same thing for NOT age > 20. (age ≤ 19) I can't remember whether I was thinking that I should answer which one *was* right or not...

### Correct answer:

IF ((NOT age < 12 AND age > 20))
{
	DISPLAY("Teenager")
}
ELSE
{
	DISPLAY("Not a Teenager")
}

This answer does *not* work (meaning it is right) because it is impossible for something to be both less than 12 and greater than 20, so that will always be false, but it's surrounded in NOT(), so it will always be true.

---

##Question 2: Cash Register (Moderate)

Stan is attempting to simulate the response of a cash register. He will accept two decimal values: the price of the item being purchased and the amount of money given by the customer. Assuming a 7% tax rate, Stan wants to calculate the amount of change the customer should receive and display that amount. If the customer gives an amount that is less than the price being bought after-tax, "NOT ENOUGH MONEY" should be displayed.

Which of the following code segments will correctly complete Stan's calculator simulator? Assume that input takes in value between 0 and 10000.

### My answer:

priceOfItem ← INPUT() 
customerPayment ← INPUT()

taxPrice ← priceOfItem x .07 + priceOfItem

IF (taxPrice > customerPayment) 
{
     DISPLAY ("NOT ENOUGH MONEY")
}
ELSE
{
     DISPLAY (taxPrice - customerPayment)
}

This answer is wrong because we know that the customer has paid at least as much money as they were supposed to, meaning customerPayment is greater than taxPrice. If we subtract customerPayment from taxPrice, the value will be the amount of change owed, but it will be negative.

### Correct answer:

priceOfItem ← INPUT() 
customerPayment ← INPUT() 

taxPrice ← priceOfItem x .07 + priceOfItem

IF (taxPrice > customerPayment) 
{
     DISPLAY ("NOT ENOUGH MONEY")
}
ELSE
{
    DISPLAY (customerPayment - taxPrice)
}

This is the same thing, but the customerPayment and taxPrice are reversed.

---

## Question 3: Iterative Arithmetic (Difficult)

The code segment below is intended to determine the quotient and remainder of two integers when big is divided by small. Assume big is always greater than or equal to small and both are positive integers.

Example:

- big = 36
- small = 5
- quotient should be 7 and remainder should be 1.

---

big  ← INPUT()
small ← INPUT()
times ← 0

REPEAT UNTIL (big < small)
{
     big ← big - small
     times ← times + 1
}

quotient ←  INSERT STATEMENT
remainder ← INSERT STATEMENT

DISPLAY(quotient)
DISPLAY(remainder)

Which of the following is the correct implementation for quotient and remainder?

### My answer:

quotient ← times - 1
remainder ← big

I think I assumed that because big was less than small, that big was also less than 0, but it wasn't. I got confused about the loop part of it, and it made me think that I had to subtract 1 from times because it would have run once too many. This was mentioned in the explanation as well; it said that if big was less than 0, it would be necessary to subtract one, but it's not.

They used this table to explain how to figure this question out:

big     36	31	26	21	16	11	6	1
times	0	1	2	3	4	5	6	7

### Correct answer:

quotient ← times
remainder ← big

---

## Question 4: Searching for a List in a Value (Difficult)

Consider the following procedures (A, B, and C) that are all designed to search the list aList for the value 23 and return the index in the list where 23 is found. If the number 23 is not in the list, -1 should be returned.

### My answer:

A and B work correctly but C does not work correctly. I remember I was very confused by this question because I was unsure about how the index return part worked. I didn't see a way that the index was being updated each round of running the loop, so I thought it must be returning the value it was assigned when the variable was declared. In the end, I basically guessed.

### Correct answer:

A and B work correctly but C does not work correctly.

All three procedures work correctly. The index variable is assigned at the beginning and it is incremented every time the loop runs. In programs A and B, if 23 is found, the loop stops and the value is immediately returned. But in program C, this index gets assigned to a variable which is returned after the loop has finished running. This makes A and B more efficient than C, and thus answer B is correct.

---

## Question 5: Ice Cream Company Wants a New Flavor (Moderate)

A major ice cream company has asked the following question via a social media post: “If you were to create a new ice cream flavor, what would it be?” The company intends to narrow down the responses to ten choices and then have its followers vote on their favorite flavor from that list. The company plans to use this input to introduce the winning flavor in its ice cream lineup in the near future.

Which of the following terms best describes the problem-solving process that the ice cream company is using?

### My answer:

I answered citizen science. I remember being really confident about my answer, too. I thought I remembered another question that had a simalar thing about doing a survey and they called it citizen science. I can't find that question on the test, so maybe it was on one of the homework quizzes. Unfortunately, I was wrong.

### Correct answer:

The correct answer is crowdsourcing. They define this as *the process of getting data from a large number of people on the internet*. According to the question, "citizen science is when scientific research is distributed among multiple people, many of whom may not even be scientists. The individuals contribute their time and/or their computing power with the goal of helping to carry out the research. The ice cream company is not performing scientific research, so it is not using citizen science." I think this perfectly explains the difference between citzen science and crowdsourcing, but to sum it up, citizen science is when many people contribute their own resources to do scientific research, and crowdsourcing is when many people contribute data not related to scientific research.
