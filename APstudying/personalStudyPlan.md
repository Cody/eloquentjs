# My Study Plan

**Questions:**
1. On which Big Ideas did you have the most difficulty on the practice test?
2. Which resources do you plan to use to study the Big Ideas listed above?
3. How will you use these resources to improve your score?

## On which Big Ideas did you have the most difficulty on the practice test?

**The percentage of questions I got wrong in the various big ideas/sections is as follows:**
1. 12.5%
2. 18.75%
3. 16.67%
4. 0%
5. 13.33%

**The *number* of questions I got wrong in each section is as follows:**
1. 1/8
2. 3/16
3. 4/24
4. 0/7
5. 2/15

As you can see, there is no *one* category that jumps out as something I particularly need to work on. However, the most concerning ones might be big ideas 3 and 4; they were the two categories that I got the highest percent wrong in, and also the highest *number* of questions wrong.

---
## Which resources do you plan to use to study the Big Ideas listed above?

The **first** thing I want to do is correct the answers I got wrong on the test.

**Then**, I would want to find more information about key terms in each section. For section 4, for example, these are:

- The Internet
- Fault tolerance
- Parallel and Distributed computing

I would probably mostly use Wikipedia and Code.org for this.

**Finally**, assuming I still have time left to study, I would do as many questions as I could from Albert.io about the topics I got wrong.

---
## How will you use these resources to improve your score?

Correcting the answers I got wrong on the test would target *exactly* what I need to study and give me a more comprehensive picture of my abilities and knowledge - and where my blind spots are.

Finding more information about vocabulary from the different sections will deepen my knowledge and narrow those aformentioned blind spots.

Doing practice questions would let me instantly correct myself if I got one wrong, get my mind more used to answering questoins like that, and prepare me for the next practice test.
