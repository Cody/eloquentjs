from random import randint

correct = 0;

for i in range(10):
    num1 = randint(1, 10)
    num2 = randint(1, 10)
    question = "What is " + str(num1) + " times " + str(num2) + "? "
    answer = int(input(question))
    if answer == num1 * num2:
        print("That's right - well done.")
        correct = correct + 1
    else:
        print("No, I'm afraid the answer is " + str(num1 * num2) + ".")

print("You got " + str(correct) + "/10 questions right. That's " + str(correct*10) + "% accuracy.");
