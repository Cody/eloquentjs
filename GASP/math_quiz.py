from random import randint

num1 = randint(1, 10)
num2 = randint(1, 10)
question = "What is " + str(num1) + " times " + str(num2) + "? "
answer = 0
while answer != num1 * num2:
    answer = int(input(question))
    if num1 * num2 != answer:
        print("No, I'm afraid you are WRONG.")

print("That's right - well done.")
