from gasp import *

x = int(input("Enter a number for x: "))
y = int(input("Enter a number for y: "))

begin_graphics()

Circle((x, y), 40)
Circle((x - 15, y + 10), 5)
Circle((x + 15, y + 10), 5)

Line((x, y + 10), (x - 10, y - 10))
Line((x - 10, y - 10), (x + 10, y - 10))

Arc((x, y), 30, 225, 315)

update_when('key_pressed')
end_graphics()
